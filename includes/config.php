<?php
error_reporting(E_ERROR);
 // error_reporting (E_NONE);
//error_reporting (E_ALL);

define ('APPNAME', 'mmrb');
define ('SERVERNAME', 'oprek.com');
define ('DBNAME', 'mmrb');
define ('MODULE_DIR', 'modules');
define ('SYSTEM_DIR', 'system');

define ('APPTITLE', 'Pendataan Muda-mudi Riung Bandung');
define ('SESSNAME', APPNAME.'_session');

define ('NAMA_TAHUN', '2012');
define ('NAMA_UNIT', 'DITJEN DIKDAS');
define ('NAMA_UNIT_SINGKAT', 'DIKDAS');
define ('NAMA_UNIT_KODE', 'dikdas');
define ('KODE_UNIT', '03');
define ('KPA_TINGKAT', 1);
define ('JABATAN_PENANDATANGAN', 4);
define ('JUDUL_NO_7', 'output');

define ('SATKER', '170002');			//ex: 414726
define ('SPPNUMBER_DIGITS', 4);			//ex: 4
define ('KODE_KEGIATAN_PERKANTORAN', '10.07.03.0002');
define ('KODE_KEGIATAN_BIROKRASI', '10.07.03.0003');
define ('KODE_NOMOR_SPP', 'SET');

//SPP SPECIFICS//
define ('NOMOR_DIPA','0037.0/023-03.01/-/2011');
define ('TANGGAL_DIPA', '20 Desember 2010');
define ('TAHUN_ANGGARAN', '2011');
define ('REVISI_RKA', 1);
define ('REVISI_DIPA', 2);
define ('REVISI_CLOSE',0);
define ('REVISI_OPEN', 1);

//BACKGROUND IMAGES
define ('SATKERTYPE', 'PUSAT');
//define ('SATKERTYPE', 'DEKON');
//define ('NAMA_UNIT', 'BPSDM & PMP');
ini_set("precision", 15);

//SPM NETWORK
define ('UNITSATKER', '02303');
define ('DOKUMEN_URL', 'http://dokumen.diknassulut.org/');
//define ('DOKUMEN_STORAGE', 'D:/Projects/localhost/dokumen/set/sardo');
?>
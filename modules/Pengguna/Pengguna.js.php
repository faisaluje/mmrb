mmrb.PenggunaModule = Ext.extend(Xond.Module, {
	name: 'pengguna',
	title: 'Pengguna',
	panel: 'null',
	subMenu: 'Aplikasi',
	create: function(){
	
		var apiPengguna = '/rest/Pengguna.';
	
		mmrb.RecordSetPengguna = [
			{ name: 'PenggunaId', type: 'float'  } ,
			{ name: 'Username', type: 'string'  } ,
			{ name: 'Password', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JabatanId', type: 'float'  }     
		];
		
		mmrb.RecordObjPengguna = Ext.data.Record.create(mmrb.RecordSetPengguna);
		
		/*mmrb.StorePengguna = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PenggunaId',
				root: 'rows',
				totalProperty: 'results',
				fields: mmrb.RecordSetPengguna        
			}),
			//url: './Pengguna/fetch.json',
			autoLoad: false,        
			url: './data/Pengguna.json',
			
			sortInfo: {field: 'PenggunaId', direction: "ASC"}
		});*/
		
		mmrb.StorePengguna = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PenggunaId',
				root: 'rows',
				totalProperty: 'results',
				fields: mmrb.RecordSetPengguna 
			}),
			writer: new Ext.data.JsonWriter({
				returnJson: true,
				writeAllFields: false,
				encode: true,
				listful: true
			}),
			proxy: new Ext.data.HttpProxy({
				disableCaching: false, // no need for cache busting when loading via Ajax
				api: {
					read:    apiPengguna + 'view', 
					create:  apiPengguna + 'create',
					update:  apiPengguna + 'update',
					destroy: apiPengguna + 'destroy'
				},
				listeners: {
					exception: function(proxy, type, action, o, res, arg){
						var msg = res.message ? res.message : Ext.decode(res.responseText).message;
						// ideally an app would provide a less intrusive message display
						Ext.Msg.alert('Server Error', msg);
					},
					write : function ( store, action, result, res, rs ) {
						console.log(action);
						console.log(result);
						console.log(res);
						console.log(rs);
						var aksi = '';
						switch (action) {
							case 'create':
								aksi = 'dibuat';
								break;
							case 'update':
								aksi = 'dimutakhirkan';
								break;        
							case 'destroy':
								aksi = 'dihancurkan';
								break;       
						}
						Xond.msg('Info', 'Data berhasil ' + aksi);
					}
				}
			}),
			autoLoad: false, 
			restful: true,
			autoSave: false,
			sortInfo: {field: 'PenggunaId', direction: "ASC"}
			});
	
		mmrb.RecordSetJabatan = [
			{name: 'JabatanId', type: 'integer'},
			{name: 'Nama', type: 'string'},
			{name: 'Deskripsi', type: 'string'}
		];
		
		mmrb.LookupStoreJabatan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JabatanId',
				root: 'rows',
				totalProperty: 'results',
				fields: mmrb.RecordSetJabatan        
			}),
			//url: './Jabatan/fetch.json',
			autoLoad: true,        
			url: './data/Jabatan.json',
			
			sortInfo: {field: 'JabatanId', direction: "ASC"}
		});
		
		
		var penggunaGrid = new Ext.grid.GridPanel({
			tableName: 'Pengguna',
			id: 'grid-pengguna',
			region: 'center',
			title: 'Daftar Pengguna',
			iconCls: 'icon-grid',
			store: mmrb.StorePengguna,
			border: false,
			frame: false,
			loadMask: true,
			animCollapse: false,
			tbar: [{
				text: 'Tambah',
				iconCls: 'add',
				handler: function(){
					// Bersihkan
					penggunaPanel.getForm().reset();
				}
			},'-',{
				text: 'Edit',
				iconCls: 'edit',
				handler: function(){
					// Cek, ada pemilihan row gak?
					var sel = penggunaGrid.getSelectionModel().getSelected();
					
					if (!sel) {
						// Kalau tidak ada row dipilih, kasih tau user
						Ext.Msg.alert('Error', 'Mohon pilih dulu salah satu');							
					} else { 
						//console.log(sel.data);
						
						// Isi form dengan data record tersebut
						penggunaPanel.getForm().setValues(sel.data);
					}
				}
			},'-',{
				text: 'Hapus',
				iconCls: 'remove',
				handler: function() {
					
					var sel = penggunaGrid.getSelectionModel().getSelections();
					if (!sel) {
						// Kalau tidak ada row dipilih, kasih tau user
						Ext.Msg.alert('Error', 'Mohon pilih dulu salah satu data yg akan dihapus');							
						return;
					}			
					
					Ext.MessageBox.confirm('Konfirmasi', 'Yakin akan menghapus ?', function(btn){
						
						if (btn == 'no'){							
							
							return false;
						
						} else {
						
							for (i = 0; i < sel.length; i++) {
								record = sel[i];						
								mmrb.StorePengguna.remove(record);
							}
							mmrb.StorePengguna.save();

						}
					});
				}	
			},'->',{
				text: 'Refresh',
				iconCls: 'refresh',
				handler: function(btn){    
					mmrb.StorePengguna.reload();
				}
			}],
			columns : [{                
				header: 'PenggunaId',
				width: 40,
				sortable: true, 
				dataIndex: 'PenggunaId'

			},{                
				header: 'Username',
				width: 100,
				sortable: true, 
				dataIndex: 'Username'

			},{                
				header: 'Password',
				width: 150,
				sortable: true, 
				dataIndex: 'Password'

			},{                
				header: 'Nama',
				width: 120,
				sortable: true, 
				dataIndex: 'Nama'

			},{                
				header: 'Jabatan',
				width: 80,
				renderer:
					function(v,params,record) {
						record = mmrb.LookupStoreJabatan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}                        
					},
				sortable: true, 
				dataIndex: 'JabatanId'
			}]
		});
	
		var penggunaPanel = new Ext.FormPanel({
			id: 'formPengguna', 
			url: 'abah-back.php',
			method: 'POST',
			frame: true,
			defaults: {
				xtype: 'textfield',
				anchor: '100%'				
			},
			items: [{
				xtype: 'hidden',						
				name: 'PenggunaId'
			},{
				fieldLabel: 'Nama',						
				name: 'Nama'
			},{						
				fieldLabel: 'Username',
				name: 'Username'
			},{
				fieldLabel: 'Password',					
				name: 'Password',
				inputType: 'password'						
			},{
				xtype: 'numberfield',
				fieldLabel: 'Jabatan',						
				name: 'JabatanId'							
			}],
			buttons: [{
				text: 'Simpan',
				iconCls: 'save',
				handler: function(btn){    
					var pengguna = new mmrb.RecordObjPengguna(penggunaPanel.getForm().getValues());       
					mmrb.StorePengguna.add(pengguna);
					mmrb.StorePengguna.save();
				}
			}]
		});
		
		penggunaGrid.on('rowclick', function(grid, rowIndex, e){
			
			var sel = grid.getSelectionModel().getSelected();
			
			// Isi form dengan data record tersebut
			penggunaPanel.getForm().setValues(sel.data);
		});
		
		this.panel = new Xond.layout.UL({
			id: 'pengguna-tab',
			title: 'Daftar Pengguna',
			upperPanel: penggunaPanel,
			lowerPanel: penggunaGrid
		});
		
		setTimeout(function(){
			mmrb.StorePengguna.load();
		}, 1000);
		
		this.fireEvent('create', this);
		
		return this.panel;
	}, 
	init: function(){
		this.addEvents({
			'create' : true
		});
	}
});
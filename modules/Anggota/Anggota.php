<?php

class AnggotaModule extends XondModule{
	var $component;
	
	function AnggotaModule(){
		parent::__construct();		
//		$this->registerUi('default', $this->LIBDIR);
		$this->registerUi('Anggota');
		$this->setMinifyUi(false);
		$this->setTitle('Anggota');
//		$this->setParams();
	}
	
	function execFetchAnggota(){
		$c = new Criteria();
		if($_REQUEST["kelompokId"]){
			$c->add(AnggotaPeer::KELOMPOK_ID, $_REQUEST["kelompokId"]);
		}
		$c->addAscendingOrderByColumn(AnggotaPeer::NAMA);
		$count = AnggotaPeer::doCount($c);
		
		$start = $_REQUEST['start'] ? $_REQUEST['start'] : 0;
		$limit = $_REQUEST['limit'] ? $_REQUEST['limit'] : 20;
		$c->setOffset($start);
		$c->setLimit($limit);
		
		$anggotas = getArray(AnggotaPeer::doSelect($c));
		foreach ($anggotas as $a){
			$start++;
			$arr = $a;
			$arr["No"] = $start;
			
			$outArr[] = $arr;
		}
// 		print_r($outArr);die;
		$this->write(tableJson($outArr, $count, AnggotaPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	}
	
	function execSave(){
		$anggota = AnggotaPeer::retrieveByPK($_REQUEST["AnggotaId"]);
		$uploadDir = 'images/';
		$theFileName = '';
		
		if(is_uploaded_file($_FILES['Foto']['tmp_name'])){
			$anggotaName = str_replace(" ", "_", $anggota->getNama());
			$uploadFile = $uploadDir.$anggotaName.'.png';
			
			if(move_uploaded_file($_FILES['Foto']['tmp_name'], $uploadFile)){
				$success = 'true';
			}else{
				$success = 'falsed';
			}
		}else{
			$success = 'false';
		}
				
		if(!is_object($anggota)){
			$anggota = new Anggota();
			$anggota->setAnggotaId(shortenUuid(generateUuid()));
		}
		
		$anggota->fromArray($_REQUEST, BasePeer::TYPE_PHPNAME);
// 		print_r($anggota);die;
		if ($anggota->save() || $success == 'true') {
			die("{ 'success': 'true', 'message': 'Berhasil menyimpan rincian anggota' }");
		} else {
			die("{ 'success': 'false', 'message': 'Tidak ada perubahan' }");
		}
	}
}

?>
mmrb.AnggotaModule = Ext.extend(Xond.Module, {
    name: 'anggota',
    title: 'Anggota',
    panel: null,
    create: function(){
		var recordSetAnggota = [{
			name: 'AnggotaId',
			type: 'string'
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'JenisKelamin',
			type: 'float'
		}, {
			name: 'TempatLahir',
			type: 'string'
		}, {
			name: 'TanggalLahir',
			type: 'date',
			dateFormat: 'm/d/y'
		}, {
			name: 'PekerjaanId',
			type: 'float'
		}, {
			name: 'Keterangan',
			type: 'string'
		}, {
			name: 'Alamat',
			type: 'string'
		}, {
			name: 'Tlp',
			type: 'string'
		}, {
			name: 'Foto',
			type: 'string'
		}, {
			name: 'KelompokId',
			type: 'float'
		}, {
			name: 'No',
			type: 'float'
		}];
		var recordObjAnggota = Ext.data.Record.create(recordSetAnggota);
		var storeAnggota = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'AnggotaId',
				root: 'rows',
				totalProperty: 'results',
				fields: recordSetAnggota
			}),
			autoLoad: false,
			url: './anggota/fetchAnggota.json',
			sortInfo: {
				field: 'Nama',
				direction: "ASC"
			}
		});
		var recordSetPekerjaan = [{
			name: 'PekerjaanId',
			type: 'float'
		}, {
			name: 'Nama',
			type: 'string'
		}];
		var recordObjPekerjaan = Ext.data.Record.create(recordSetPekerjaan);
		var lookupStorePekerjaan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PekerjaanId',
				root: 'rows',
				totalProperty: 'results',
				fields: recordSetPekerjaan
			}),
			autoLoad: true,
			url: './data/Pekerjaan.json',
			sortInfo: {
				field: 'PekerjaanId',
				direction: "ASC"
			}
		});
		
		function JenisKelamin(v) {
			// if (v == 0) return "A"
			// if (v == 1) return "B"
			console.log(v);
		}
		
		var gridAnggota = new Ext.grid.GridPanel({
			id: 'grid-anggota',
			region: 'center',
			store: storeAnggota,
			title: 'Daftar Anggota',
			iconCls: 'icon-grid',
			sm: new Ext.grid.RowSelectionModel(),
			// height: 450,
			viewConfig: {
				forceFit: true
			},
			loadMask: true,
			tbar: [{
				xtype: 'kelompokcombo',
				listeners : {
					select: function(thiscombo, record, index) {
						storeAnggota.reload({
							params : {
								kelompokId : thiscombo.getValue()
							}
						})
					}
				}
			},'->',{
				text: 'Refresh',
				tooltip: 'Klik untuk merefresh grid',
				iconCls: 'refresh',
				handler: function(btn){
					storeAnggota.reload();
				}
			}],
			columns: [{
				header: 'No.',
				width: 20,
				align: 'left',
				sortable: true,
				dataIndex: 'No'
			},{
				header: 'Nama',
				width: 100,
				sortable: true,
				dataIndex: 'Nama'
			},{
				header: 'Tempat Lahir',
				width: 50,
				sortable: true,
				dataIndex: 'TempatLahir'
			},{                
                header: 'TanggalLahir',
                width: 80,
                renderer: Ext.util.Format.dateRenderer('m/d/Y'),
                sortable: true, 
                dataIndex: 'TanggalLahir'

            },{                
                header: 'Alamat',
                width: 150,
                sortable: true, 
                dataIndex: 'Alamat'

            },{                
                header: 'Tlp',
                width: 100,
                sortable: true, 
                dataIndex: 'Tlp'

            }],
			bbar: new Ext.PagingToolbar({
				pageSize: 20,
				store: storeAnggota,
				displayInfo: true,
				displayMsg: 'Menampilkan {0} - {1} dari {2}',
				emptyMsg: 'Data tidak ditemukan'
			})
		});
		
		gridAnggota.on('rowclick', function(grid, rowIndex, e){
			
			var sel = grid.getSelectionModel().getSelected();
			
			// Isi form dengan data record tersebut
			gridForm.getForm().setValues(sel.data);
		});
		
		var template = new Ext.Template([
			'<table><tr><td><img src="images/{Foto}" width="100%" height="100%"/></td></tr></table>'
		]);
		
		var fotoPanel = new Ext.Panel({
			xtype: 'fotopengguna',
			itemId: 'fotopenggunaPanel',
			region: 'south',
			tpl: template
		});
		
		gridAnggota.getSelectionModel().on('rowselect', function(sm, rowIdx, r) {
			template.overwrite(fotoPanel.body, r.data);
		});
		
		Ext.apply(fotoPanel, {
			bodyStyle: {
				background: '#ffffff',
				padding: '2px'
			},
			html: 'Silahkan pilih baris untuk menampilkan foto'
		});
		
		var gridForm = new Ext.FormPanel({
			id: 'anggota-form',
			frame: true,
			labelAlign: 'left',
			title: 'Rincian Anggota',
			layout: {
				type:'hbox',			
				align:'stretch'
			},
			bodyStyle:'padding:2px',
			// region: 'center',
			url: 'Anggota/save.php',
			method: 'POST',
			layout: 'column',
			fileUpload: true,
			items: [{
				columnWidth: 0.15,
				// layout: 'fit',
				items: fotoPanel
			},{
				columnWidth: 0.85,
				xtype: 'fieldset',
				labelWidth: 90,
				defaults: {width: 140, border:false},    // Default config options for child items
				defaultType: 'textfield',
				autoHeight: true,
				bodyStyle: Ext.isIE ? 'padding:0 0 5px 10px;' : 'padding:10px 10px;',
				border: false,
				style: {
					"margin-left": "10px", // when you add custom margin in IE 6...
					"margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
				},
				items: [{
					xtype: 'hidden',
					name: 'AnggotaId'
				},{
					fieldLabel: 'Nama',
					name: 'Nama',
					allowBlank:false,
					width: 200
				},{
					xtype: 'radiogroup',
					columns: 'auto',
					width: 200,
					allowBlank:false,
					fieldLabel: 'Jenis Kelamin',
					items: [{
						name: 'JenisKelamin',
						inputValue: 1,
						boxLabel: 'Laki-laki'
					},{
						name: 'JenisKelamin',
						inputValue: 2,
						boxLabel: 'Perempuan'
					}]
				},{
					fieldLabel: 'Tempat Lahir',
					name: 'TempatLahir',
					allowBlank:false,
					width: 100
				},{
					xtype: 'datefield',
					fieldLabel: 'Tanggal Lahir',
					allowBlank:false,
					name: 'TanggalLahir'
				},{
					xtype: 'combo',
					fieldLabel: 'Pekerjaan',
					allowBlank:false,
					// name: 'PekerjaanId',
					hiddenName : 'PekerjaanId',
					store: lookupStorePekerjaan,
					displayField: 'Nama',
					valueField: 'PekerjaanId',
					align: 'left',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText: 'Pilih...',
					editable: false,
					selectOnFocus: true
				},{
					fieldLabel: 'Keterangan',
					name: 'Keterangan',
					allowBlank:false,
					width: 200
				},{
					fieldLabel: 'Alamat',
					allowBlank:false,
					name: 'Alamat',
					width: 250
				},{
					fieldLabel: 'Telepon',
					allowBlank:false,
					name: 'Tlp'
				},{
					xtype: 'fileuploadfield',
					fieldLabel: 'Foto',
					emptyText: 'Pilih Foto.. .png',
					allowBlank: false,
					name: 'Foto',
					width: 250
				}, {
					xtype: 'kelompokcombo',
					hiddenName: 'KelompokId'
				}]
			}],
			buttons: [{
				text: 'Simpan',
				iconCls: 'save',
				handler: function(){
					// console.log(gridForm.getForm().getValues());
					if(gridForm.getForm().isValid()){
						gridForm.getForm().submit({
							waitMsg: 'Mengirim data ...',
							success: function(fp, o){
								if (o.result.success == 'true') {
									// Kalau berhasil simpan, reload gridnya
									storeAnggota.reload();
									// Kasih tahu bahwa simpan berhasil ke user
									Ext.Msg.alert('Info', '' + o.result.message + '.');
								} else {
									Ext.Msg.alert('Error', '' + o.result.message + '.');
								}
							},
							failure: function(response, o){                                            
								Ext.Msg.alert('Gagal', o.result.message );
							}
						});
					}else{
						Xond.msg('Peringatan', 'Data harus lengkap!');
					}
				}
			}]
		});
		
		this.panel = new Xond.layout.UL({
			id: 'anggota-tab',
			title: 'Daftar Anggota',
			upperPanel: gridForm,
			lowerPanel: gridAnggota
		});
		
		setTimeout(function(){
            storeAnggota.load();
        }, 1000);
		this.fireEvent('create', this);
		
		return this.panel;
	},
	init: function(){
		this.addEvents({
			'create' : true
		});
	}
});
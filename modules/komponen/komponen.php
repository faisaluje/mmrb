<?php

class KomponenModule extends XondModule{
	var $component;
	
	function KomponenModule(){
		parent::__construct();
		
		$this->setTitle('Komponen');
	}
	
	function execPekerjaan(){
		$c = new Criteria();
		$pekerja = PekerjaanPeer::doSelect($c);
		$rowCount = PekerjaanPeer::doCount($c);
		$fieldName = PekerjaanPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson(getArray($pekerja), $rowCount, $fieldName));
	}
}

?>
<?php
require_once 'startup.php';

$komponen = $_GET['table'];

switch ($komponen){
	case 'pekerjaan':
		pekerjaan();
		break;
	case 'kelompok':
		kelompok();
		break;
	case 'anggota':
		anggota();
		break;
	case 'upload':
		upload();
		break;
	case 'pesan':
		pesan();
		break;
}

function pekerjaan(){
	$c = new Criteria();
	$pekerjaan =  PekerjaanPeer::doSelect($c);
	$rowCount = PekerjaanPeer::doCount($c);
	$fieldName = PekerjaanPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
	echo tableJson(getArray($pekerjaan), $rowCount, $fieldName);
}

function kelompok(){
	$c = new Criteria();
	$kelompok =  KelompokPeer::doSelect($c);
	$rowCount = KelompokPeer::doCount($c);
	$fieldName = KelompokPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
	echo tableJson(getArray($kelompok), $rowCount, $fieldName);
}

function anggota(){
	try{
		$c = new Anggota();
		$c->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
		$fileName = str_replace(" ", "_", $c->getNama());
		$anggotaId = shortenUuid(generateUuid());
		$c->setAnggotaId($anggotaId);
		$c->setFoto($fileName .'.png');
// 		print_r($c);die;
		if($c->save()){
			echo("{ success : true }");
		}else{
			echo("{ success : false }");
		}
	}catch(Exception $e){
		echo(sprintf("{ success : false , message : '".$e->getMessage()."'}"));
	}
}

function upload(){
	$uploadDir = 'images/';
	$theFileName = '';
	
	if(is_uploaded_file($_FILES['foto']['tmp_name'])){
		$n = new Anggota();
		$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
		$anggota = str_replace(" ", "_", $n->getNama());
		$uploadFile = $uploadDir . $anggota . '.png';
		if(move_uploaded_file($_FILES['foto']['tmp_name'], $uploadFile)){
			$success = 'true';
		}else{
			$success = 'falsed';
		}
	}else{
		$success = 'false';
	}
	$result = sprintf("{ success : %s, message : '%s' }", $success, $success);
	echo($result);
}

function pesan(){
	$b = new Criteria();
	$b->add(AnggotaPeer::NAMA, $_GET["nama"]);
	$anggota = AnggotaPeer::doSelect($b);
	foreach ($anggota as $a){
		$anggotaId = $a->getAnggotaId();
	}
	try{
		$c = new Pesan();
		$c->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
		$c->setAnggotaId($anggotaId);
		if($c->save()){
			echo("{ success : true }");
		}else{
			echo("{ success : false }");
		}
	}catch(Exception $e){
		echo(sprintf("{ success : false }"));
	}
}

?>
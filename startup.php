<?php

define ('ROOT', dirname(__FILE__));
define ('D', DIRECTORY_SEPARATOR);
define ('P', PATH_SEPARATOR); 
define ('SYSDIR', ROOT.D.'system'.D);

set_include_path( 
	ROOT.D.'build'.D.'classes'.P.
	ROOT.D.'..'.D.'xond'.P.
	get_include_path()
);

require_once 'propel/Propel.php';
Propel::init("build/conf/mmrb-conf.php");
set_include_path("build/classes" . PATH_SEPARATOR . get_include_path());

//require_once 'propel/engine/database/model/NameGenerator.php';
require_once 'PHPExcel/PHPExcel.php';
require_once 'library/functions.php';
require_once 'includes/config.php';

spl_autoload_register("class_autoloader");

//Propel::init(ROOT.D.'build'.D.'conf'.D.APPNAME.'-conf.php');

session_start();
?>
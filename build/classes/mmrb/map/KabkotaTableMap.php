<?php



/**
 * This class defines the structure of the 'kabkota' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.mmrb.map
 */
class KabkotaTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'mmrb.map.KabkotaTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('kabkota');
		$this->setPhpName('Kabkota');
		$this->setClassname('Kabkota');
		$this->setPackage('mmrb');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('KABKOTA_ID', 'KabkotaId', 'INTEGER', true, null, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 50, null);
		$this->addForeignKey('PROPINSI_ID', 'PropinsiId', 'INTEGER', 'propinsi', 'PROPINSI_ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Propinsi', 'Propinsi', RelationMap::MANY_TO_ONE, array('propinsi_id' => 'propinsi_id', ), null, null);
    $this->addRelation('Daerah', 'Daerah', RelationMap::ONE_TO_MANY, array('kabkota_id' => 'kabkota_id', ), null, null);
	} // buildRelations()

} // KabkotaTableMap

<?php



/**
 * This class defines the structure of the 'daerah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.mmrb.map
 */
class DaerahTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'mmrb.map.DaerahTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('daerah');
		$this->setPhpName('Daerah');
		$this->setClassname('Daerah');
		$this->setPackage('mmrb');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('DAERAH_ID', 'DaerahId', 'INTEGER', true, null, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 50, null);
		$this->addColumn('ALAMAT', 'Alamat', 'VARCHAR', true, 255, null);
		$this->addForeignKey('KABKOTA_ID', 'KabkotaId', 'INTEGER', 'kabkota', 'KABKOTA_ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Kabkota', 'Kabkota', RelationMap::MANY_TO_ONE, array('kabkota_id' => 'kabkota_id', ), null, null);
    $this->addRelation('Desa', 'Desa', RelationMap::ONE_TO_MANY, array('daerah_id' => 'daerah_id', ), null, null);
	} // buildRelations()

} // DaerahTableMap

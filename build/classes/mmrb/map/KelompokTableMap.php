<?php



/**
 * This class defines the structure of the 'kelompok' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.mmrb.map
 */
class KelompokTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'mmrb.map.KelompokTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('kelompok');
		$this->setPhpName('Kelompok');
		$this->setClassname('Kelompok');
		$this->setPackage('mmrb');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('KELOMPOK_ID', 'KelompokId', 'INTEGER', true, null, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 50, null);
		$this->addColumn('ALAMAT', 'Alamat', 'VARCHAR', true, 255, null);
		$this->addForeignKey('DESA_ID', 'DesaId', 'INTEGER', 'desa', 'DESA_ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Desa', 'Desa', RelationMap::MANY_TO_ONE, array('desa_id' => 'desa_id', ), null, null);
    $this->addRelation('Anggota', 'Anggota', RelationMap::ONE_TO_MANY, array('kelompok_id' => 'kelompok_id', ), null, null);
	} // buildRelations()

} // KelompokTableMap

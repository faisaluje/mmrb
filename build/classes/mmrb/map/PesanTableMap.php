<?php



/**
 * This class defines the structure of the 'pesan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.mmrb.map
 */
class PesanTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'mmrb.map.PesanTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('pesan');
		$this->setPhpName('Pesan');
		$this->setClassname('Pesan');
		$this->setPackage('mmrb');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('PESAN_ID', 'PesanId', 'INTEGER', true, null, null);
		$this->addColumn('ISI', 'Isi', 'LONGVARCHAR', true, null, null);
		$this->addForeignKey('ANGGOTA_ID', 'AnggotaId', 'VARCHAR', 'anggota', 'ANGGOTA_ID', true, 22, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Anggota', 'Anggota', RelationMap::MANY_TO_ONE, array('anggota_id' => 'anggota_id', ), null, null);
	} // buildRelations()

} // PesanTableMap

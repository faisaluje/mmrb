<?php



/**
 * This class defines the structure of the 'anggota' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.mmrb.map
 */
class AnggotaTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'mmrb.map.AnggotaTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('anggota');
		$this->setPhpName('Anggota');
		$this->setClassname('Anggota');
		$this->setPackage('mmrb');
		$this->setUseIdGenerator(false);
		// columns
		$this->addPrimaryKey('ANGGOTA_ID', 'AnggotaId', 'VARCHAR', true, 22, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 255, null);
		$this->addColumn('JENIS_KELAMIN', 'JenisKelamin', 'INTEGER', true, 2, null);
		$this->addColumn('TEMPAT_LAHIR', 'TempatLahir', 'VARCHAR', true, 50, null);
		$this->addColumn('TANGGAL_LAHIR', 'TanggalLahir', 'DATE', true, null, null);
		$this->addForeignKey('PEKERJAAN_ID', 'PekerjaanId', 'INTEGER', 'pekerjaan', 'PEKERJAAN_ID', true, null, null);
		$this->addColumn('KETERANGAN', 'Keterangan', 'VARCHAR', true, 255, null);
		$this->addColumn('ALAMAT', 'Alamat', 'VARCHAR', true, 255, null);
		$this->addColumn('TLP', 'Tlp', 'VARCHAR', true, 50, null);
		$this->addColumn('FOTO', 'Foto', 'VARCHAR', true, 255, null);
		$this->addForeignKey('KELOMPOK_ID', 'KelompokId', 'INTEGER', 'kelompok', 'KELOMPOK_ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Kelompok', 'Kelompok', RelationMap::MANY_TO_ONE, array('kelompok_id' => 'kelompok_id', ), null, null);
    $this->addRelation('Pekerjaan', 'Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id' => 'pekerjaan_id', ), null, null);
    $this->addRelation('Pesan', 'Pesan', RelationMap::ONE_TO_MANY, array('anggota_id' => 'anggota_id', ), null, null);
	} // buildRelations()

} // AnggotaTableMap

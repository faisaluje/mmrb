<?php



/**
 * This class defines the structure of the 'pengguna' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.mmrb.map
 */
class PenggunaTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'mmrb.map.PenggunaTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('pengguna');
		$this->setPhpName('Pengguna');
		$this->setClassname('Pengguna');
		$this->setPackage('mmrb');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('PENGGUNA_ID', 'PenggunaId', 'INTEGER', true, null, null);
		$this->addColumn('USERNAME', 'Username', 'VARCHAR', true, 255, null);
		$this->addColumn('PASSWORD', 'Password', 'VARCHAR', true, 255, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 255, null);
		$this->addForeignKey('JABATAN_ID', 'JabatanId', 'INTEGER', 'jabatan', 'JABATAN_ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Jabatan', 'Jabatan', RelationMap::MANY_TO_ONE, array('jabatan_id' => 'jabatan_id', ), null, null);
	} // buildRelations()

} // PenggunaTableMap

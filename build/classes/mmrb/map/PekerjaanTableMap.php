<?php



/**
 * This class defines the structure of the 'pekerjaan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.mmrb.map
 */
class PekerjaanTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'mmrb.map.PekerjaanTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('pekerjaan');
		$this->setPhpName('Pekerjaan');
		$this->setClassname('Pekerjaan');
		$this->setPackage('mmrb');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('PEKERJAAN_ID', 'PekerjaanId', 'INTEGER', true, null, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 255, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Anggota', 'Anggota', RelationMap::ONE_TO_MANY, array('pekerjaan_id' => 'pekerjaan_id', ), null, null);
	} // buildRelations()

} // PekerjaanTableMap

<?php


/**
 * Base class that represents a query for the 'pesan' table.
 *
 * 
 *
 * @method     PesanQuery orderByPesanId($order = Criteria::ASC) Order by the pesan_id column
 * @method     PesanQuery orderByIsi($order = Criteria::ASC) Order by the isi column
 * @method     PesanQuery orderByAnggotaId($order = Criteria::ASC) Order by the anggota_id column
 *
 * @method     PesanQuery groupByPesanId() Group by the pesan_id column
 * @method     PesanQuery groupByIsi() Group by the isi column
 * @method     PesanQuery groupByAnggotaId() Group by the anggota_id column
 *
 * @method     PesanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     PesanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     PesanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     PesanQuery leftJoinAnggota($relationAlias = null) Adds a LEFT JOIN clause to the query using the Anggota relation
 * @method     PesanQuery rightJoinAnggota($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Anggota relation
 * @method     PesanQuery innerJoinAnggota($relationAlias = null) Adds a INNER JOIN clause to the query using the Anggota relation
 *
 * @method     Pesan findOne(PropelPDO $con = null) Return the first Pesan matching the query
 * @method     Pesan findOneOrCreate(PropelPDO $con = null) Return the first Pesan matching the query, or a new Pesan object populated from the query conditions when no match is found
 *
 * @method     Pesan findOneByPesanId(int $pesan_id) Return the first Pesan filtered by the pesan_id column
 * @method     Pesan findOneByIsi(string $isi) Return the first Pesan filtered by the isi column
 * @method     Pesan findOneByAnggotaId(string $anggota_id) Return the first Pesan filtered by the anggota_id column
 *
 * @method     array findByPesanId(int $pesan_id) Return Pesan objects filtered by the pesan_id column
 * @method     array findByIsi(string $isi) Return Pesan objects filtered by the isi column
 * @method     array findByAnggotaId(string $anggota_id) Return Pesan objects filtered by the anggota_id column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BasePesanQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BasePesanQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Pesan', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new PesanQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    PesanQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof PesanQuery) {
			return $criteria;
		}
		$query = new PesanQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Pesan|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = PesanPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(PesanPeer::PESAN_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(PesanPeer::PESAN_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the pesan_id column
	 * 
	 * @param     int|array $pesanId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function filterByPesanId($pesanId = null, $comparison = null)
	{
		if (is_array($pesanId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(PesanPeer::PESAN_ID, $pesanId, $comparison);
	}

	/**
	 * Filter the query on the isi column
	 * 
	 * @param     string $isi The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function filterByIsi($isi = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($isi)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $isi)) {
				$isi = str_replace('*', '%', $isi);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PesanPeer::ISI, $isi, $comparison);
	}

	/**
	 * Filter the query on the anggota_id column
	 * 
	 * @param     string $anggotaId The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function filterByAnggotaId($anggotaId = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($anggotaId)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $anggotaId)) {
				$anggotaId = str_replace('*', '%', $anggotaId);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PesanPeer::ANGGOTA_ID, $anggotaId, $comparison);
	}

	/**
	 * Filter the query by a related Anggota object
	 *
	 * @param     Anggota $anggota  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function filterByAnggota($anggota, $comparison = null)
	{
		return $this
			->addUsingAlias(PesanPeer::ANGGOTA_ID, $anggota->getAnggotaId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Anggota relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function joinAnggota($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Anggota');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Anggota');
		}
		
		return $this;
	}

	/**
	 * Use the Anggota relation Anggota object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AnggotaQuery A secondary query class using the current class as primary query
	 */
	public function useAnggotaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAnggota($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Anggota', 'AnggotaQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Pesan $pesan Object to remove from the list of results
	 *
	 * @return    PesanQuery The current query, for fluid interface
	 */
	public function prune($pesan = null)
	{
		if ($pesan) {
			$this->addUsingAlias(PesanPeer::PESAN_ID, $pesan->getPesanId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BasePesanQuery

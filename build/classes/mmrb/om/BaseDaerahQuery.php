<?php


/**
 * Base class that represents a query for the 'daerah' table.
 *
 * 
 *
 * @method     DaerahQuery orderByDaerahId($order = Criteria::ASC) Order by the daerah_id column
 * @method     DaerahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     DaerahQuery orderByAlamat($order = Criteria::ASC) Order by the alamat column
 * @method     DaerahQuery orderByKabkotaId($order = Criteria::ASC) Order by the kabkota_id column
 *
 * @method     DaerahQuery groupByDaerahId() Group by the daerah_id column
 * @method     DaerahQuery groupByNama() Group by the nama column
 * @method     DaerahQuery groupByAlamat() Group by the alamat column
 * @method     DaerahQuery groupByKabkotaId() Group by the kabkota_id column
 *
 * @method     DaerahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     DaerahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     DaerahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     DaerahQuery leftJoinKabkota($relationAlias = null) Adds a LEFT JOIN clause to the query using the Kabkota relation
 * @method     DaerahQuery rightJoinKabkota($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Kabkota relation
 * @method     DaerahQuery innerJoinKabkota($relationAlias = null) Adds a INNER JOIN clause to the query using the Kabkota relation
 *
 * @method     DaerahQuery leftJoinDesa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Desa relation
 * @method     DaerahQuery rightJoinDesa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Desa relation
 * @method     DaerahQuery innerJoinDesa($relationAlias = null) Adds a INNER JOIN clause to the query using the Desa relation
 *
 * @method     Daerah findOne(PropelPDO $con = null) Return the first Daerah matching the query
 * @method     Daerah findOneOrCreate(PropelPDO $con = null) Return the first Daerah matching the query, or a new Daerah object populated from the query conditions when no match is found
 *
 * @method     Daerah findOneByDaerahId(int $daerah_id) Return the first Daerah filtered by the daerah_id column
 * @method     Daerah findOneByNama(string $nama) Return the first Daerah filtered by the nama column
 * @method     Daerah findOneByAlamat(string $alamat) Return the first Daerah filtered by the alamat column
 * @method     Daerah findOneByKabkotaId(int $kabkota_id) Return the first Daerah filtered by the kabkota_id column
 *
 * @method     array findByDaerahId(int $daerah_id) Return Daerah objects filtered by the daerah_id column
 * @method     array findByNama(string $nama) Return Daerah objects filtered by the nama column
 * @method     array findByAlamat(string $alamat) Return Daerah objects filtered by the alamat column
 * @method     array findByKabkotaId(int $kabkota_id) Return Daerah objects filtered by the kabkota_id column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BaseDaerahQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseDaerahQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Daerah', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new DaerahQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    DaerahQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof DaerahQuery) {
			return $criteria;
		}
		$query = new DaerahQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Daerah|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = DaerahPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(DaerahPeer::DAERAH_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(DaerahPeer::DAERAH_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the daerah_id column
	 * 
	 * @param     int|array $daerahId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByDaerahId($daerahId = null, $comparison = null)
	{
		if (is_array($daerahId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(DaerahPeer::DAERAH_ID, $daerahId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(DaerahPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the alamat column
	 * 
	 * @param     string $alamat The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByAlamat($alamat = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($alamat)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $alamat)) {
				$alamat = str_replace('*', '%', $alamat);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(DaerahPeer::ALAMAT, $alamat, $comparison);
	}

	/**
	 * Filter the query on the kabkota_id column
	 * 
	 * @param     int|array $kabkotaId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByKabkotaId($kabkotaId = null, $comparison = null)
	{
		if (is_array($kabkotaId)) {
			$useMinMax = false;
			if (isset($kabkotaId['min'])) {
				$this->addUsingAlias(DaerahPeer::KABKOTA_ID, $kabkotaId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($kabkotaId['max'])) {
				$this->addUsingAlias(DaerahPeer::KABKOTA_ID, $kabkotaId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(DaerahPeer::KABKOTA_ID, $kabkotaId, $comparison);
	}

	/**
	 * Filter the query by a related Kabkota object
	 *
	 * @param     Kabkota $kabkota  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByKabkota($kabkota, $comparison = null)
	{
		return $this
			->addUsingAlias(DaerahPeer::KABKOTA_ID, $kabkota->getKabkotaId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Kabkota relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function joinKabkota($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Kabkota');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Kabkota');
		}
		
		return $this;
	}

	/**
	 * Use the Kabkota relation Kabkota object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KabkotaQuery A secondary query class using the current class as primary query
	 */
	public function useKabkotaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinKabkota($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Kabkota', 'KabkotaQuery');
	}

	/**
	 * Filter the query by a related Desa object
	 *
	 * @param     Desa $desa  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function filterByDesa($desa, $comparison = null)
	{
		return $this
			->addUsingAlias(DaerahPeer::DAERAH_ID, $desa->getDaerahId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Desa relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function joinDesa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Desa');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Desa');
		}
		
		return $this;
	}

	/**
	 * Use the Desa relation Desa object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DesaQuery A secondary query class using the current class as primary query
	 */
	public function useDesaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinDesa($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Desa', 'DesaQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Daerah $daerah Object to remove from the list of results
	 *
	 * @return    DaerahQuery The current query, for fluid interface
	 */
	public function prune($daerah = null)
	{
		if ($daerah) {
			$this->addUsingAlias(DaerahPeer::DAERAH_ID, $daerah->getDaerahId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseDaerahQuery

<?php


/**
 * Base class that represents a row from the 'anggota' table.
 *
 * 
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BaseAnggota extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'AnggotaPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        AnggotaPeer
	 */
	protected static $peer;

	/**
	 * The value for the anggota_id field.
	 * @var        string
	 */
	protected $anggota_id;

	/**
	 * The value for the nama field.
	 * @var        string
	 */
	protected $nama;

	/**
	 * The value for the jenis_kelamin field.
	 * @var        int
	 */
	protected $jenis_kelamin;

	/**
	 * The value for the tempat_lahir field.
	 * @var        string
	 */
	protected $tempat_lahir;

	/**
	 * The value for the tanggal_lahir field.
	 * @var        string
	 */
	protected $tanggal_lahir;

	/**
	 * The value for the pekerjaan_id field.
	 * @var        int
	 */
	protected $pekerjaan_id;

	/**
	 * The value for the keterangan field.
	 * @var        string
	 */
	protected $keterangan;

	/**
	 * The value for the alamat field.
	 * @var        string
	 */
	protected $alamat;

	/**
	 * The value for the tlp field.
	 * @var        string
	 */
	protected $tlp;

	/**
	 * The value for the foto field.
	 * @var        string
	 */
	protected $foto;

	/**
	 * The value for the kelompok_id field.
	 * @var        int
	 */
	protected $kelompok_id;

	/**
	 * @var        Kelompok
	 */
	protected $aKelompok;

	/**
	 * @var        Pekerjaan
	 */
	protected $aPekerjaan;

	/**
	 * @var        array Pesan[] Collection to store aggregation of Pesan objects.
	 */
	protected $collPesans;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [anggota_id] column value.
	 * 
	 * @return     string
	 */
	public function getAnggotaId()
	{
		return $this->anggota_id;
	}

	/**
	 * Get the [nama] column value.
	 * 
	 * @return     string
	 */
	public function getNama()
	{
		return $this->nama;
	}

	/**
	 * Get the [jenis_kelamin] column value.
	 * 
	 * @return     int
	 */
	public function getJenisKelamin()
	{
		return $this->jenis_kelamin;
	}

	/**
	 * Get the [tempat_lahir] column value.
	 * 
	 * @return     string
	 */
	public function getTempatLahir()
	{
		return $this->tempat_lahir;
	}

	/**
	 * Get the [optionally formatted] temporal [tanggal_lahir] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getTanggalLahir($format = '%x')
	{
		if ($this->tanggal_lahir === null) {
			return null;
		}


		if ($this->tanggal_lahir === '0000-00-00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->tanggal_lahir);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->tanggal_lahir, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [pekerjaan_id] column value.
	 * 
	 * @return     int
	 */
	public function getPekerjaanId()
	{
		return $this->pekerjaan_id;
	}

	/**
	 * Get the [keterangan] column value.
	 * 
	 * @return     string
	 */
	public function getKeterangan()
	{
		return $this->keterangan;
	}

	/**
	 * Get the [alamat] column value.
	 * 
	 * @return     string
	 */
	public function getAlamat()
	{
		return $this->alamat;
	}

	/**
	 * Get the [tlp] column value.
	 * 
	 * @return     string
	 */
	public function getTlp()
	{
		return $this->tlp;
	}

	/**
	 * Get the [foto] column value.
	 * 
	 * @return     string
	 */
	public function getFoto()
	{
		return $this->foto;
	}

	/**
	 * Get the [kelompok_id] column value.
	 * 
	 * @return     int
	 */
	public function getKelompokId()
	{
		return $this->kelompok_id;
	}

	/**
	 * Set the value of [anggota_id] column.
	 * 
	 * @param      string $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setAnggotaId($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->anggota_id !== $v) {
			$this->anggota_id = $v;
			$this->modifiedColumns[] = AnggotaPeer::ANGGOTA_ID;
		}

		return $this;
	} // setAnggotaId()

	/**
	 * Set the value of [nama] column.
	 * 
	 * @param      string $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setNama($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = AnggotaPeer::NAMA;
		}

		return $this;
	} // setNama()

	/**
	 * Set the value of [jenis_kelamin] column.
	 * 
	 * @param      int $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setJenisKelamin($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->jenis_kelamin !== $v) {
			$this->jenis_kelamin = $v;
			$this->modifiedColumns[] = AnggotaPeer::JENIS_KELAMIN;
		}

		return $this;
	} // setJenisKelamin()

	/**
	 * Set the value of [tempat_lahir] column.
	 * 
	 * @param      string $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setTempatLahir($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->tempat_lahir !== $v) {
			$this->tempat_lahir = $v;
			$this->modifiedColumns[] = AnggotaPeer::TEMPAT_LAHIR;
		}

		return $this;
	} // setTempatLahir()

	/**
	 * Sets the value of [tanggal_lahir] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setTanggalLahir($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->tanggal_lahir !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->tanggal_lahir !== null && $tmpDt = new DateTime($this->tanggal_lahir)) ? $tmpDt->format('Y-m-d') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->tanggal_lahir = ($dt ? $dt->format('Y-m-d') : null);
				$this->modifiedColumns[] = AnggotaPeer::TANGGAL_LAHIR;
			}
		} // if either are not null

		return $this;
	} // setTanggalLahir()

	/**
	 * Set the value of [pekerjaan_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setPekerjaanId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->pekerjaan_id !== $v) {
			$this->pekerjaan_id = $v;
			$this->modifiedColumns[] = AnggotaPeer::PEKERJAAN_ID;
		}

		if ($this->aPekerjaan !== null && $this->aPekerjaan->getPekerjaanId() !== $v) {
			$this->aPekerjaan = null;
		}

		return $this;
	} // setPekerjaanId()

	/**
	 * Set the value of [keterangan] column.
	 * 
	 * @param      string $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setKeterangan($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = AnggotaPeer::KETERANGAN;
		}

		return $this;
	} // setKeterangan()

	/**
	 * Set the value of [alamat] column.
	 * 
	 * @param      string $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setAlamat($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->alamat !== $v) {
			$this->alamat = $v;
			$this->modifiedColumns[] = AnggotaPeer::ALAMAT;
		}

		return $this;
	} // setAlamat()

	/**
	 * Set the value of [tlp] column.
	 * 
	 * @param      string $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setTlp($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->tlp !== $v) {
			$this->tlp = $v;
			$this->modifiedColumns[] = AnggotaPeer::TLP;
		}

		return $this;
	} // setTlp()

	/**
	 * Set the value of [foto] column.
	 * 
	 * @param      string $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setFoto($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->foto !== $v) {
			$this->foto = $v;
			$this->modifiedColumns[] = AnggotaPeer::FOTO;
		}

		return $this;
	} // setFoto()

	/**
	 * Set the value of [kelompok_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Anggota The current object (for fluent API support)
	 */
	public function setKelompokId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->kelompok_id !== $v) {
			$this->kelompok_id = $v;
			$this->modifiedColumns[] = AnggotaPeer::KELOMPOK_ID;
		}

		if ($this->aKelompok !== null && $this->aKelompok->getKelompokId() !== $v) {
			$this->aKelompok = null;
		}

		return $this;
	} // setKelompokId()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->anggota_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
			$this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->jenis_kelamin = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->tempat_lahir = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->tanggal_lahir = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->pekerjaan_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
			$this->keterangan = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->alamat = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->tlp = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->foto = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->kelompok_id = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 11; // 11 = AnggotaPeer::NUM_COLUMNS - AnggotaPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Anggota object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aPekerjaan !== null && $this->pekerjaan_id !== $this->aPekerjaan->getPekerjaanId()) {
			$this->aPekerjaan = null;
		}
		if ($this->aKelompok !== null && $this->kelompok_id !== $this->aKelompok->getKelompokId()) {
			$this->aKelompok = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AnggotaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = AnggotaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aKelompok = null;
			$this->aPekerjaan = null;
			$this->collPesans = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AnggotaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				AnggotaQuery::create()
					->filterByPrimaryKey($this->getPrimaryKey())
					->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AnggotaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				AnggotaPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aKelompok !== null) {
				if ($this->aKelompok->isModified() || $this->aKelompok->isNew()) {
					$affectedRows += $this->aKelompok->save($con);
				}
				$this->setKelompok($this->aKelompok);
			}

			if ($this->aPekerjaan !== null) {
				if ($this->aPekerjaan->isModified() || $this->aPekerjaan->isNew()) {
					$affectedRows += $this->aPekerjaan->save($con);
				}
				$this->setPekerjaan($this->aPekerjaan);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$criteria = $this->buildCriteria();
					$pk = BasePeer::doInsert($criteria, $con);
					$affectedRows += 1;
					$this->setNew(false);
				} else {
					$affectedRows += AnggotaPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collPesans !== null) {
				foreach ($this->collPesans as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aKelompok !== null) {
				if (!$this->aKelompok->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aKelompok->getValidationFailures());
				}
			}

			if ($this->aPekerjaan !== null) {
				if (!$this->aPekerjaan->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPekerjaan->getValidationFailures());
				}
			}


			if (($retval = AnggotaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collPesans !== null) {
					foreach ($this->collPesans as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AnggotaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getAnggotaId();
				break;
			case 1:
				return $this->getNama();
				break;
			case 2:
				return $this->getJenisKelamin();
				break;
			case 3:
				return $this->getTempatLahir();
				break;
			case 4:
				return $this->getTanggalLahir();
				break;
			case 5:
				return $this->getPekerjaanId();
				break;
			case 6:
				return $this->getKeterangan();
				break;
			case 7:
				return $this->getAlamat();
				break;
			case 8:
				return $this->getTlp();
				break;
			case 9:
				return $this->getFoto();
				break;
			case 10:
				return $this->getKelompokId();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $includeForeignObjects = false)
	{
		$keys = AnggotaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getAnggotaId(),
			$keys[1] => $this->getNama(),
			$keys[2] => $this->getJenisKelamin(),
			$keys[3] => $this->getTempatLahir(),
			$keys[4] => $this->getTanggalLahir(),
			$keys[5] => $this->getPekerjaanId(),
			$keys[6] => $this->getKeterangan(),
			$keys[7] => $this->getAlamat(),
			$keys[8] => $this->getTlp(),
			$keys[9] => $this->getFoto(),
			$keys[10] => $this->getKelompokId(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aKelompok) {
				$result['Kelompok'] = $this->aKelompok->toArray($keyType, $includeLazyLoadColumns, true);
			}
			if (null !== $this->aPekerjaan) {
				$result['Pekerjaan'] = $this->aPekerjaan->toArray($keyType, $includeLazyLoadColumns, true);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AnggotaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setAnggotaId($value);
				break;
			case 1:
				$this->setNama($value);
				break;
			case 2:
				$this->setJenisKelamin($value);
				break;
			case 3:
				$this->setTempatLahir($value);
				break;
			case 4:
				$this->setTanggalLahir($value);
				break;
			case 5:
				$this->setPekerjaanId($value);
				break;
			case 6:
				$this->setKeterangan($value);
				break;
			case 7:
				$this->setAlamat($value);
				break;
			case 8:
				$this->setTlp($value);
				break;
			case 9:
				$this->setFoto($value);
				break;
			case 10:
				$this->setKelompokId($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AnggotaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setAnggotaId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setJenisKelamin($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTempatLahir($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTanggalLahir($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPekerjaanId($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKeterangan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setAlamat($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setTlp($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setFoto($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKelompokId($arr[$keys[10]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(AnggotaPeer::DATABASE_NAME);

		if ($this->isColumnModified(AnggotaPeer::ANGGOTA_ID)) $criteria->add(AnggotaPeer::ANGGOTA_ID, $this->anggota_id);
		if ($this->isColumnModified(AnggotaPeer::NAMA)) $criteria->add(AnggotaPeer::NAMA, $this->nama);
		if ($this->isColumnModified(AnggotaPeer::JENIS_KELAMIN)) $criteria->add(AnggotaPeer::JENIS_KELAMIN, $this->jenis_kelamin);
		if ($this->isColumnModified(AnggotaPeer::TEMPAT_LAHIR)) $criteria->add(AnggotaPeer::TEMPAT_LAHIR, $this->tempat_lahir);
		if ($this->isColumnModified(AnggotaPeer::TANGGAL_LAHIR)) $criteria->add(AnggotaPeer::TANGGAL_LAHIR, $this->tanggal_lahir);
		if ($this->isColumnModified(AnggotaPeer::PEKERJAAN_ID)) $criteria->add(AnggotaPeer::PEKERJAAN_ID, $this->pekerjaan_id);
		if ($this->isColumnModified(AnggotaPeer::KETERANGAN)) $criteria->add(AnggotaPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(AnggotaPeer::ALAMAT)) $criteria->add(AnggotaPeer::ALAMAT, $this->alamat);
		if ($this->isColumnModified(AnggotaPeer::TLP)) $criteria->add(AnggotaPeer::TLP, $this->tlp);
		if ($this->isColumnModified(AnggotaPeer::FOTO)) $criteria->add(AnggotaPeer::FOTO, $this->foto);
		if ($this->isColumnModified(AnggotaPeer::KELOMPOK_ID)) $criteria->add(AnggotaPeer::KELOMPOK_ID, $this->kelompok_id);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AnggotaPeer::DATABASE_NAME);
		$criteria->add(AnggotaPeer::ANGGOTA_ID, $this->anggota_id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     string
	 */
	public function getPrimaryKey()
	{
		return $this->getAnggotaId();
	}

	/**
	 * Generic method to set the primary key (anggota_id column).
	 *
	 * @param      string $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setAnggotaId($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getAnggotaId();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Anggota (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{
		$copyObj->setAnggotaId($this->anggota_id);
		$copyObj->setNama($this->nama);
		$copyObj->setJenisKelamin($this->jenis_kelamin);
		$copyObj->setTempatLahir($this->tempat_lahir);
		$copyObj->setTanggalLahir($this->tanggal_lahir);
		$copyObj->setPekerjaanId($this->pekerjaan_id);
		$copyObj->setKeterangan($this->keterangan);
		$copyObj->setAlamat($this->alamat);
		$copyObj->setTlp($this->tlp);
		$copyObj->setFoto($this->foto);
		$copyObj->setKelompokId($this->kelompok_id);

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getPesans() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addPesan($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Anggota Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     AnggotaPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AnggotaPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Kelompok object.
	 *
	 * @param      Kelompok $v
	 * @return     Anggota The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setKelompok(Kelompok $v = null)
	{
		if ($v === null) {
			$this->setKelompokId(NULL);
		} else {
			$this->setKelompokId($v->getKelompokId());
		}

		$this->aKelompok = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Kelompok object, it will not be re-added.
		if ($v !== null) {
			$v->addAnggota($this);
		}

		return $this;
	}


	/**
	 * Get the associated Kelompok object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Kelompok The associated Kelompok object.
	 * @throws     PropelException
	 */
	public function getKelompok(PropelPDO $con = null)
	{
		if ($this->aKelompok === null && ($this->kelompok_id !== null)) {
			$this->aKelompok = KelompokQuery::create()->findPk($this->kelompok_id, $con);
			/* The following can be used additionally to
				 guarantee the related object contains a reference
				 to this object.  This level of coupling may, however, be
				 undesirable since it could result in an only partially populated collection
				 in the referenced object.
				 $this->aKelompok->addAnggotas($this);
			 */
		}
		return $this->aKelompok;
	}

	/**
	 * Declares an association between this object and a Pekerjaan object.
	 *
	 * @param      Pekerjaan $v
	 * @return     Anggota The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setPekerjaan(Pekerjaan $v = null)
	{
		if ($v === null) {
			$this->setPekerjaanId(NULL);
		} else {
			$this->setPekerjaanId($v->getPekerjaanId());
		}

		$this->aPekerjaan = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Pekerjaan object, it will not be re-added.
		if ($v !== null) {
			$v->addAnggota($this);
		}

		return $this;
	}


	/**
	 * Get the associated Pekerjaan object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Pekerjaan The associated Pekerjaan object.
	 * @throws     PropelException
	 */
	public function getPekerjaan(PropelPDO $con = null)
	{
		if ($this->aPekerjaan === null && ($this->pekerjaan_id !== null)) {
			$this->aPekerjaan = PekerjaanQuery::create()->findPk($this->pekerjaan_id, $con);
			/* The following can be used additionally to
				 guarantee the related object contains a reference
				 to this object.  This level of coupling may, however, be
				 undesirable since it could result in an only partially populated collection
				 in the referenced object.
				 $this->aPekerjaan->addAnggotas($this);
			 */
		}
		return $this->aPekerjaan;
	}

	/**
	 * Clears out the collPesans collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addPesans()
	 */
	public function clearPesans()
	{
		$this->collPesans = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collPesans collection.
	 *
	 * By default this just sets the collPesans collection to an empty array (like clearcollPesans());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initPesans()
	{
		$this->collPesans = new PropelObjectCollection();
		$this->collPesans->setModel('Pesan');
	}

	/**
	 * Gets an array of Pesan objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Anggota is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Pesan[] List of Pesan objects
	 * @throws     PropelException
	 */
	public function getPesans($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collPesans || null !== $criteria) {
			if ($this->isNew() && null === $this->collPesans) {
				// return empty collection
				$this->initPesans();
			} else {
				$collPesans = PesanQuery::create(null, $criteria)
					->filterByAnggota($this)
					->find($con);
				if (null !== $criteria) {
					return $collPesans;
				}
				$this->collPesans = $collPesans;
			}
		}
		return $this->collPesans;
	}

	/**
	 * Returns the number of related Pesan objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Pesan objects.
	 * @throws     PropelException
	 */
	public function countPesans(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collPesans || null !== $criteria) {
			if ($this->isNew() && null === $this->collPesans) {
				return 0;
			} else {
				$query = PesanQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByAnggota($this)
					->count($con);
			}
		} else {
			return count($this->collPesans);
		}
	}

	/**
	 * Method called to associate a Pesan object to this object
	 * through the Pesan foreign key attribute.
	 *
	 * @param      Pesan $l Pesan
	 * @return     void
	 * @throws     PropelException
	 */
	public function addPesan(Pesan $l)
	{
		if ($this->collPesans === null) {
			$this->initPesans();
		}
		if (!$this->collPesans->contains($l)) { // only add it if the **same** object is not already associated
			$this->collPesans[]= $l;
			$l->setAnggota($this);
		}
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->anggota_id = null;
		$this->nama = null;
		$this->jenis_kelamin = null;
		$this->tempat_lahir = null;
		$this->tanggal_lahir = null;
		$this->pekerjaan_id = null;
		$this->keterangan = null;
		$this->alamat = null;
		$this->tlp = null;
		$this->foto = null;
		$this->kelompok_id = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collPesans) {
				foreach ((array) $this->collPesans as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		$this->collPesans = null;
		$this->aKelompok = null;
		$this->aPekerjaan = null;
	}

	/**
	 * Catches calls to virtual methods
	 */
	public function __call($name, $params)
	{
		if (preg_match('/get(\w+)/', $name, $matches)) {
			$virtualColumn = $matches[1];
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
			// no lcfirst in php<5.3...
			$virtualColumn[0] = strtolower($virtualColumn[0]);
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
		}
		return parent::__call($name, $params);
	}

} // BaseAnggota

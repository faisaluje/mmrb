<?php


/**
 * Base class that represents a query for the 'pekerjaan' table.
 *
 * 
 *
 * @method     PekerjaanQuery orderByPekerjaanId($order = Criteria::ASC) Order by the pekerjaan_id column
 * @method     PekerjaanQuery orderByNama($order = Criteria::ASC) Order by the nama column
 *
 * @method     PekerjaanQuery groupByPekerjaanId() Group by the pekerjaan_id column
 * @method     PekerjaanQuery groupByNama() Group by the nama column
 *
 * @method     PekerjaanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     PekerjaanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     PekerjaanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     PekerjaanQuery leftJoinAnggota($relationAlias = null) Adds a LEFT JOIN clause to the query using the Anggota relation
 * @method     PekerjaanQuery rightJoinAnggota($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Anggota relation
 * @method     PekerjaanQuery innerJoinAnggota($relationAlias = null) Adds a INNER JOIN clause to the query using the Anggota relation
 *
 * @method     Pekerjaan findOne(PropelPDO $con = null) Return the first Pekerjaan matching the query
 * @method     Pekerjaan findOneOrCreate(PropelPDO $con = null) Return the first Pekerjaan matching the query, or a new Pekerjaan object populated from the query conditions when no match is found
 *
 * @method     Pekerjaan findOneByPekerjaanId(int $pekerjaan_id) Return the first Pekerjaan filtered by the pekerjaan_id column
 * @method     Pekerjaan findOneByNama(string $nama) Return the first Pekerjaan filtered by the nama column
 *
 * @method     array findByPekerjaanId(int $pekerjaan_id) Return Pekerjaan objects filtered by the pekerjaan_id column
 * @method     array findByNama(string $nama) Return Pekerjaan objects filtered by the nama column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BasePekerjaanQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BasePekerjaanQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Pekerjaan', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new PekerjaanQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    PekerjaanQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof PekerjaanQuery) {
			return $criteria;
		}
		$query = new PekerjaanQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Pekerjaan|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = PekerjaanPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    PekerjaanQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(PekerjaanPeer::PEKERJAAN_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    PekerjaanQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(PekerjaanPeer::PEKERJAAN_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the pekerjaan_id column
	 * 
	 * @param     int|array $pekerjaanId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PekerjaanQuery The current query, for fluid interface
	 */
	public function filterByPekerjaanId($pekerjaanId = null, $comparison = null)
	{
		if (is_array($pekerjaanId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(PekerjaanPeer::PEKERJAAN_ID, $pekerjaanId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PekerjaanQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PekerjaanPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query by a related Anggota object
	 *
	 * @param     Anggota $anggota  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PekerjaanQuery The current query, for fluid interface
	 */
	public function filterByAnggota($anggota, $comparison = null)
	{
		return $this
			->addUsingAlias(PekerjaanPeer::PEKERJAAN_ID, $anggota->getPekerjaanId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Anggota relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PekerjaanQuery The current query, for fluid interface
	 */
	public function joinAnggota($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Anggota');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Anggota');
		}
		
		return $this;
	}

	/**
	 * Use the Anggota relation Anggota object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AnggotaQuery A secondary query class using the current class as primary query
	 */
	public function useAnggotaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAnggota($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Anggota', 'AnggotaQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Pekerjaan $pekerjaan Object to remove from the list of results
	 *
	 * @return    PekerjaanQuery The current query, for fluid interface
	 */
	public function prune($pekerjaan = null)
	{
		if ($pekerjaan) {
			$this->addUsingAlias(PekerjaanPeer::PEKERJAAN_ID, $pekerjaan->getPekerjaanId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BasePekerjaanQuery

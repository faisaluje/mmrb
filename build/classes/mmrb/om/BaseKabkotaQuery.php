<?php


/**
 * Base class that represents a query for the 'kabkota' table.
 *
 * 
 *
 * @method     KabkotaQuery orderByKabkotaId($order = Criteria::ASC) Order by the kabkota_id column
 * @method     KabkotaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     KabkotaQuery orderByPropinsiId($order = Criteria::ASC) Order by the propinsi_id column
 *
 * @method     KabkotaQuery groupByKabkotaId() Group by the kabkota_id column
 * @method     KabkotaQuery groupByNama() Group by the nama column
 * @method     KabkotaQuery groupByPropinsiId() Group by the propinsi_id column
 *
 * @method     KabkotaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     KabkotaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     KabkotaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     KabkotaQuery leftJoinPropinsi($relationAlias = null) Adds a LEFT JOIN clause to the query using the Propinsi relation
 * @method     KabkotaQuery rightJoinPropinsi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Propinsi relation
 * @method     KabkotaQuery innerJoinPropinsi($relationAlias = null) Adds a INNER JOIN clause to the query using the Propinsi relation
 *
 * @method     KabkotaQuery leftJoinDaerah($relationAlias = null) Adds a LEFT JOIN clause to the query using the Daerah relation
 * @method     KabkotaQuery rightJoinDaerah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Daerah relation
 * @method     KabkotaQuery innerJoinDaerah($relationAlias = null) Adds a INNER JOIN clause to the query using the Daerah relation
 *
 * @method     Kabkota findOne(PropelPDO $con = null) Return the first Kabkota matching the query
 * @method     Kabkota findOneOrCreate(PropelPDO $con = null) Return the first Kabkota matching the query, or a new Kabkota object populated from the query conditions when no match is found
 *
 * @method     Kabkota findOneByKabkotaId(int $kabkota_id) Return the first Kabkota filtered by the kabkota_id column
 * @method     Kabkota findOneByNama(string $nama) Return the first Kabkota filtered by the nama column
 * @method     Kabkota findOneByPropinsiId(int $propinsi_id) Return the first Kabkota filtered by the propinsi_id column
 *
 * @method     array findByKabkotaId(int $kabkota_id) Return Kabkota objects filtered by the kabkota_id column
 * @method     array findByNama(string $nama) Return Kabkota objects filtered by the nama column
 * @method     array findByPropinsiId(int $propinsi_id) Return Kabkota objects filtered by the propinsi_id column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BaseKabkotaQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseKabkotaQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Kabkota', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new KabkotaQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    KabkotaQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof KabkotaQuery) {
			return $criteria;
		}
		$query = new KabkotaQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Kabkota|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = KabkotaPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(KabkotaPeer::KABKOTA_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(KabkotaPeer::KABKOTA_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the kabkota_id column
	 * 
	 * @param     int|array $kabkotaId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function filterByKabkotaId($kabkotaId = null, $comparison = null)
	{
		if (is_array($kabkotaId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(KabkotaPeer::KABKOTA_ID, $kabkotaId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(KabkotaPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the propinsi_id column
	 * 
	 * @param     int|array $propinsiId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function filterByPropinsiId($propinsiId = null, $comparison = null)
	{
		if (is_array($propinsiId)) {
			$useMinMax = false;
			if (isset($propinsiId['min'])) {
				$this->addUsingAlias(KabkotaPeer::PROPINSI_ID, $propinsiId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($propinsiId['max'])) {
				$this->addUsingAlias(KabkotaPeer::PROPINSI_ID, $propinsiId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(KabkotaPeer::PROPINSI_ID, $propinsiId, $comparison);
	}

	/**
	 * Filter the query by a related Propinsi object
	 *
	 * @param     Propinsi $propinsi  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function filterByPropinsi($propinsi, $comparison = null)
	{
		return $this
			->addUsingAlias(KabkotaPeer::PROPINSI_ID, $propinsi->getPropinsiId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Propinsi relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function joinPropinsi($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Propinsi');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Propinsi');
		}
		
		return $this;
	}

	/**
	 * Use the Propinsi relation Propinsi object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PropinsiQuery A secondary query class using the current class as primary query
	 */
	public function usePropinsiQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinPropinsi($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Propinsi', 'PropinsiQuery');
	}

	/**
	 * Filter the query by a related Daerah object
	 *
	 * @param     Daerah $daerah  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function filterByDaerah($daerah, $comparison = null)
	{
		return $this
			->addUsingAlias(KabkotaPeer::KABKOTA_ID, $daerah->getKabkotaId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Daerah relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function joinDaerah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Daerah');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Daerah');
		}
		
		return $this;
	}

	/**
	 * Use the Daerah relation Daerah object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DaerahQuery A secondary query class using the current class as primary query
	 */
	public function useDaerahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinDaerah($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Daerah', 'DaerahQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Kabkota $kabkota Object to remove from the list of results
	 *
	 * @return    KabkotaQuery The current query, for fluid interface
	 */
	public function prune($kabkota = null)
	{
		if ($kabkota) {
			$this->addUsingAlias(KabkotaPeer::KABKOTA_ID, $kabkota->getKabkotaId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseKabkotaQuery

<?php


/**
 * Base class that represents a query for the 'jabatan' table.
 *
 * 
 *
 * @method     JabatanQuery orderByJabatanId($order = Criteria::ASC) Order by the jabatan_id column
 * @method     JabatanQuery orderByNama($order = Criteria::ASC) Order by the nama column
 *
 * @method     JabatanQuery groupByJabatanId() Group by the jabatan_id column
 * @method     JabatanQuery groupByNama() Group by the nama column
 *
 * @method     JabatanQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     JabatanQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     JabatanQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     JabatanQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method     JabatanQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method     JabatanQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method     Jabatan findOne(PropelPDO $con = null) Return the first Jabatan matching the query
 * @method     Jabatan findOneOrCreate(PropelPDO $con = null) Return the first Jabatan matching the query, or a new Jabatan object populated from the query conditions when no match is found
 *
 * @method     Jabatan findOneByJabatanId(int $jabatan_id) Return the first Jabatan filtered by the jabatan_id column
 * @method     Jabatan findOneByNama(string $nama) Return the first Jabatan filtered by the nama column
 *
 * @method     array findByJabatanId(int $jabatan_id) Return Jabatan objects filtered by the jabatan_id column
 * @method     array findByNama(string $nama) Return Jabatan objects filtered by the nama column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BaseJabatanQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseJabatanQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Jabatan', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new JabatanQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    JabatanQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof JabatanQuery) {
			return $criteria;
		}
		$query = new JabatanQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Jabatan|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = JabatanPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    JabatanQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(JabatanPeer::JABATAN_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    JabatanQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(JabatanPeer::JABATAN_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the jabatan_id column
	 * 
	 * @param     int|array $jabatanId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    JabatanQuery The current query, for fluid interface
	 */
	public function filterByJabatanId($jabatanId = null, $comparison = null)
	{
		if (is_array($jabatanId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(JabatanPeer::JABATAN_ID, $jabatanId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    JabatanQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(JabatanPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query by a related Pengguna object
	 *
	 * @param     Pengguna $pengguna  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    JabatanQuery The current query, for fluid interface
	 */
	public function filterByPengguna($pengguna, $comparison = null)
	{
		return $this
			->addUsingAlias(JabatanPeer::JABATAN_ID, $pengguna->getJabatanId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Pengguna relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    JabatanQuery The current query, for fluid interface
	 */
	public function joinPengguna($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Pengguna');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Pengguna');
		}
		
		return $this;
	}

	/**
	 * Use the Pengguna relation Pengguna object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PenggunaQuery A secondary query class using the current class as primary query
	 */
	public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinPengguna($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Pengguna', 'PenggunaQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Jabatan $jabatan Object to remove from the list of results
	 *
	 * @return    JabatanQuery The current query, for fluid interface
	 */
	public function prune($jabatan = null)
	{
		if ($jabatan) {
			$this->addUsingAlias(JabatanPeer::JABATAN_ID, $jabatan->getJabatanId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseJabatanQuery

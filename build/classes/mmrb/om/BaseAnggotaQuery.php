<?php


/**
 * Base class that represents a query for the 'anggota' table.
 *
 * 
 *
 * @method     AnggotaQuery orderByAnggotaId($order = Criteria::ASC) Order by the anggota_id column
 * @method     AnggotaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     AnggotaQuery orderByJenisKelamin($order = Criteria::ASC) Order by the jenis_kelamin column
 * @method     AnggotaQuery orderByTempatLahir($order = Criteria::ASC) Order by the tempat_lahir column
 * @method     AnggotaQuery orderByTanggalLahir($order = Criteria::ASC) Order by the tanggal_lahir column
 * @method     AnggotaQuery orderByPekerjaanId($order = Criteria::ASC) Order by the pekerjaan_id column
 * @method     AnggotaQuery orderByKeterangan($order = Criteria::ASC) Order by the keterangan column
 * @method     AnggotaQuery orderByAlamat($order = Criteria::ASC) Order by the alamat column
 * @method     AnggotaQuery orderByTlp($order = Criteria::ASC) Order by the tlp column
 * @method     AnggotaQuery orderByFoto($order = Criteria::ASC) Order by the foto column
 * @method     AnggotaQuery orderByKelompokId($order = Criteria::ASC) Order by the kelompok_id column
 *
 * @method     AnggotaQuery groupByAnggotaId() Group by the anggota_id column
 * @method     AnggotaQuery groupByNama() Group by the nama column
 * @method     AnggotaQuery groupByJenisKelamin() Group by the jenis_kelamin column
 * @method     AnggotaQuery groupByTempatLahir() Group by the tempat_lahir column
 * @method     AnggotaQuery groupByTanggalLahir() Group by the tanggal_lahir column
 * @method     AnggotaQuery groupByPekerjaanId() Group by the pekerjaan_id column
 * @method     AnggotaQuery groupByKeterangan() Group by the keterangan column
 * @method     AnggotaQuery groupByAlamat() Group by the alamat column
 * @method     AnggotaQuery groupByTlp() Group by the tlp column
 * @method     AnggotaQuery groupByFoto() Group by the foto column
 * @method     AnggotaQuery groupByKelompokId() Group by the kelompok_id column
 *
 * @method     AnggotaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     AnggotaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     AnggotaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     AnggotaQuery leftJoinKelompok($relationAlias = null) Adds a LEFT JOIN clause to the query using the Kelompok relation
 * @method     AnggotaQuery rightJoinKelompok($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Kelompok relation
 * @method     AnggotaQuery innerJoinKelompok($relationAlias = null) Adds a INNER JOIN clause to the query using the Kelompok relation
 *
 * @method     AnggotaQuery leftJoinPekerjaan($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pekerjaan relation
 * @method     AnggotaQuery rightJoinPekerjaan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pekerjaan relation
 * @method     AnggotaQuery innerJoinPekerjaan($relationAlias = null) Adds a INNER JOIN clause to the query using the Pekerjaan relation
 *
 * @method     AnggotaQuery leftJoinPesan($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pesan relation
 * @method     AnggotaQuery rightJoinPesan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pesan relation
 * @method     AnggotaQuery innerJoinPesan($relationAlias = null) Adds a INNER JOIN clause to the query using the Pesan relation
 *
 * @method     Anggota findOne(PropelPDO $con = null) Return the first Anggota matching the query
 * @method     Anggota findOneOrCreate(PropelPDO $con = null) Return the first Anggota matching the query, or a new Anggota object populated from the query conditions when no match is found
 *
 * @method     Anggota findOneByAnggotaId(string $anggota_id) Return the first Anggota filtered by the anggota_id column
 * @method     Anggota findOneByNama(string $nama) Return the first Anggota filtered by the nama column
 * @method     Anggota findOneByJenisKelamin(int $jenis_kelamin) Return the first Anggota filtered by the jenis_kelamin column
 * @method     Anggota findOneByTempatLahir(string $tempat_lahir) Return the first Anggota filtered by the tempat_lahir column
 * @method     Anggota findOneByTanggalLahir(string $tanggal_lahir) Return the first Anggota filtered by the tanggal_lahir column
 * @method     Anggota findOneByPekerjaanId(int $pekerjaan_id) Return the first Anggota filtered by the pekerjaan_id column
 * @method     Anggota findOneByKeterangan(string $keterangan) Return the first Anggota filtered by the keterangan column
 * @method     Anggota findOneByAlamat(string $alamat) Return the first Anggota filtered by the alamat column
 * @method     Anggota findOneByTlp(string $tlp) Return the first Anggota filtered by the tlp column
 * @method     Anggota findOneByFoto(string $foto) Return the first Anggota filtered by the foto column
 * @method     Anggota findOneByKelompokId(int $kelompok_id) Return the first Anggota filtered by the kelompok_id column
 *
 * @method     array findByAnggotaId(string $anggota_id) Return Anggota objects filtered by the anggota_id column
 * @method     array findByNama(string $nama) Return Anggota objects filtered by the nama column
 * @method     array findByJenisKelamin(int $jenis_kelamin) Return Anggota objects filtered by the jenis_kelamin column
 * @method     array findByTempatLahir(string $tempat_lahir) Return Anggota objects filtered by the tempat_lahir column
 * @method     array findByTanggalLahir(string $tanggal_lahir) Return Anggota objects filtered by the tanggal_lahir column
 * @method     array findByPekerjaanId(int $pekerjaan_id) Return Anggota objects filtered by the pekerjaan_id column
 * @method     array findByKeterangan(string $keterangan) Return Anggota objects filtered by the keterangan column
 * @method     array findByAlamat(string $alamat) Return Anggota objects filtered by the alamat column
 * @method     array findByTlp(string $tlp) Return Anggota objects filtered by the tlp column
 * @method     array findByFoto(string $foto) Return Anggota objects filtered by the foto column
 * @method     array findByKelompokId(int $kelompok_id) Return Anggota objects filtered by the kelompok_id column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BaseAnggotaQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseAnggotaQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Anggota', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new AnggotaQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    AnggotaQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof AnggotaQuery) {
			return $criteria;
		}
		$query = new AnggotaQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Anggota|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = AnggotaPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(AnggotaPeer::ANGGOTA_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(AnggotaPeer::ANGGOTA_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the anggota_id column
	 * 
	 * @param     string $anggotaId The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByAnggotaId($anggotaId = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($anggotaId)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $anggotaId)) {
				$anggotaId = str_replace('*', '%', $anggotaId);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::ANGGOTA_ID, $anggotaId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the jenis_kelamin column
	 * 
	 * @param     int|array $jenisKelamin The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByJenisKelamin($jenisKelamin = null, $comparison = null)
	{
		if (is_array($jenisKelamin)) {
			$useMinMax = false;
			if (isset($jenisKelamin['min'])) {
				$this->addUsingAlias(AnggotaPeer::JENIS_KELAMIN, $jenisKelamin['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($jenisKelamin['max'])) {
				$this->addUsingAlias(AnggotaPeer::JENIS_KELAMIN, $jenisKelamin['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::JENIS_KELAMIN, $jenisKelamin, $comparison);
	}

	/**
	 * Filter the query on the tempat_lahir column
	 * 
	 * @param     string $tempatLahir The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByTempatLahir($tempatLahir = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($tempatLahir)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $tempatLahir)) {
				$tempatLahir = str_replace('*', '%', $tempatLahir);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::TEMPAT_LAHIR, $tempatLahir, $comparison);
	}

	/**
	 * Filter the query on the tanggal_lahir column
	 * 
	 * @param     string|array $tanggalLahir The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByTanggalLahir($tanggalLahir = null, $comparison = null)
	{
		if (is_array($tanggalLahir)) {
			$useMinMax = false;
			if (isset($tanggalLahir['min'])) {
				$this->addUsingAlias(AnggotaPeer::TANGGAL_LAHIR, $tanggalLahir['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($tanggalLahir['max'])) {
				$this->addUsingAlias(AnggotaPeer::TANGGAL_LAHIR, $tanggalLahir['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::TANGGAL_LAHIR, $tanggalLahir, $comparison);
	}

	/**
	 * Filter the query on the pekerjaan_id column
	 * 
	 * @param     int|array $pekerjaanId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByPekerjaanId($pekerjaanId = null, $comparison = null)
	{
		if (is_array($pekerjaanId)) {
			$useMinMax = false;
			if (isset($pekerjaanId['min'])) {
				$this->addUsingAlias(AnggotaPeer::PEKERJAAN_ID, $pekerjaanId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($pekerjaanId['max'])) {
				$this->addUsingAlias(AnggotaPeer::PEKERJAAN_ID, $pekerjaanId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::PEKERJAAN_ID, $pekerjaanId, $comparison);
	}

	/**
	 * Filter the query on the keterangan column
	 * 
	 * @param     string $keterangan The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByKeterangan($keterangan = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($keterangan)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $keterangan)) {
				$keterangan = str_replace('*', '%', $keterangan);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::KETERANGAN, $keterangan, $comparison);
	}

	/**
	 * Filter the query on the alamat column
	 * 
	 * @param     string $alamat The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByAlamat($alamat = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($alamat)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $alamat)) {
				$alamat = str_replace('*', '%', $alamat);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::ALAMAT, $alamat, $comparison);
	}

	/**
	 * Filter the query on the tlp column
	 * 
	 * @param     string $tlp The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByTlp($tlp = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($tlp)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $tlp)) {
				$tlp = str_replace('*', '%', $tlp);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::TLP, $tlp, $comparison);
	}

	/**
	 * Filter the query on the foto column
	 * 
	 * @param     string $foto The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByFoto($foto = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($foto)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $foto)) {
				$foto = str_replace('*', '%', $foto);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::FOTO, $foto, $comparison);
	}

	/**
	 * Filter the query on the kelompok_id column
	 * 
	 * @param     int|array $kelompokId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByKelompokId($kelompokId = null, $comparison = null)
	{
		if (is_array($kelompokId)) {
			$useMinMax = false;
			if (isset($kelompokId['min'])) {
				$this->addUsingAlias(AnggotaPeer::KELOMPOK_ID, $kelompokId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($kelompokId['max'])) {
				$this->addUsingAlias(AnggotaPeer::KELOMPOK_ID, $kelompokId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(AnggotaPeer::KELOMPOK_ID, $kelompokId, $comparison);
	}

	/**
	 * Filter the query by a related Kelompok object
	 *
	 * @param     Kelompok $kelompok  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByKelompok($kelompok, $comparison = null)
	{
		return $this
			->addUsingAlias(AnggotaPeer::KELOMPOK_ID, $kelompok->getKelompokId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Kelompok relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function joinKelompok($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Kelompok');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Kelompok');
		}
		
		return $this;
	}

	/**
	 * Use the Kelompok relation Kelompok object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KelompokQuery A secondary query class using the current class as primary query
	 */
	public function useKelompokQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinKelompok($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Kelompok', 'KelompokQuery');
	}

	/**
	 * Filter the query by a related Pekerjaan object
	 *
	 * @param     Pekerjaan $pekerjaan  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByPekerjaan($pekerjaan, $comparison = null)
	{
		return $this
			->addUsingAlias(AnggotaPeer::PEKERJAAN_ID, $pekerjaan->getPekerjaanId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Pekerjaan relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function joinPekerjaan($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Pekerjaan');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Pekerjaan');
		}
		
		return $this;
	}

	/**
	 * Use the Pekerjaan relation Pekerjaan object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PekerjaanQuery A secondary query class using the current class as primary query
	 */
	public function usePekerjaanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinPekerjaan($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Pekerjaan', 'PekerjaanQuery');
	}

	/**
	 * Filter the query by a related Pesan object
	 *
	 * @param     Pesan $pesan  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function filterByPesan($pesan, $comparison = null)
	{
		return $this
			->addUsingAlias(AnggotaPeer::ANGGOTA_ID, $pesan->getAnggotaId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Pesan relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function joinPesan($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Pesan');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Pesan');
		}
		
		return $this;
	}

	/**
	 * Use the Pesan relation Pesan object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PesanQuery A secondary query class using the current class as primary query
	 */
	public function usePesanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinPesan($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Pesan', 'PesanQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Anggota $anggota Object to remove from the list of results
	 *
	 * @return    AnggotaQuery The current query, for fluid interface
	 */
	public function prune($anggota = null)
	{
		if ($anggota) {
			$this->addUsingAlias(AnggotaPeer::ANGGOTA_ID, $anggota->getAnggotaId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseAnggotaQuery

<?php


/**
 * Base class that represents a query for the 'propinsi' table.
 *
 * 
 *
 * @method     PropinsiQuery orderByPropinsiId($order = Criteria::ASC) Order by the propinsi_id column
 * @method     PropinsiQuery orderByNama($order = Criteria::ASC) Order by the nama column
 *
 * @method     PropinsiQuery groupByPropinsiId() Group by the propinsi_id column
 * @method     PropinsiQuery groupByNama() Group by the nama column
 *
 * @method     PropinsiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     PropinsiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     PropinsiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     PropinsiQuery leftJoinKabkota($relationAlias = null) Adds a LEFT JOIN clause to the query using the Kabkota relation
 * @method     PropinsiQuery rightJoinKabkota($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Kabkota relation
 * @method     PropinsiQuery innerJoinKabkota($relationAlias = null) Adds a INNER JOIN clause to the query using the Kabkota relation
 *
 * @method     Propinsi findOne(PropelPDO $con = null) Return the first Propinsi matching the query
 * @method     Propinsi findOneOrCreate(PropelPDO $con = null) Return the first Propinsi matching the query, or a new Propinsi object populated from the query conditions when no match is found
 *
 * @method     Propinsi findOneByPropinsiId(int $propinsi_id) Return the first Propinsi filtered by the propinsi_id column
 * @method     Propinsi findOneByNama(string $nama) Return the first Propinsi filtered by the nama column
 *
 * @method     array findByPropinsiId(int $propinsi_id) Return Propinsi objects filtered by the propinsi_id column
 * @method     array findByNama(string $nama) Return Propinsi objects filtered by the nama column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BasePropinsiQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BasePropinsiQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Propinsi', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new PropinsiQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    PropinsiQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof PropinsiQuery) {
			return $criteria;
		}
		$query = new PropinsiQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Propinsi|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = PropinsiPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    PropinsiQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(PropinsiPeer::PROPINSI_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    PropinsiQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(PropinsiPeer::PROPINSI_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the propinsi_id column
	 * 
	 * @param     int|array $propinsiId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PropinsiQuery The current query, for fluid interface
	 */
	public function filterByPropinsiId($propinsiId = null, $comparison = null)
	{
		if (is_array($propinsiId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(PropinsiPeer::PROPINSI_ID, $propinsiId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PropinsiQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PropinsiPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query by a related Kabkota object
	 *
	 * @param     Kabkota $kabkota  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PropinsiQuery The current query, for fluid interface
	 */
	public function filterByKabkota($kabkota, $comparison = null)
	{
		return $this
			->addUsingAlias(PropinsiPeer::PROPINSI_ID, $kabkota->getPropinsiId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Kabkota relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PropinsiQuery The current query, for fluid interface
	 */
	public function joinKabkota($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Kabkota');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Kabkota');
		}
		
		return $this;
	}

	/**
	 * Use the Kabkota relation Kabkota object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KabkotaQuery A secondary query class using the current class as primary query
	 */
	public function useKabkotaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinKabkota($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Kabkota', 'KabkotaQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Propinsi $propinsi Object to remove from the list of results
	 *
	 * @return    PropinsiQuery The current query, for fluid interface
	 */
	public function prune($propinsi = null)
	{
		if ($propinsi) {
			$this->addUsingAlias(PropinsiPeer::PROPINSI_ID, $propinsi->getPropinsiId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BasePropinsiQuery

<?php


/**
 * Base class that represents a query for the 'kelompok' table.
 *
 * 
 *
 * @method     KelompokQuery orderByKelompokId($order = Criteria::ASC) Order by the kelompok_id column
 * @method     KelompokQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     KelompokQuery orderByAlamat($order = Criteria::ASC) Order by the alamat column
 * @method     KelompokQuery orderByDesaId($order = Criteria::ASC) Order by the desa_id column
 *
 * @method     KelompokQuery groupByKelompokId() Group by the kelompok_id column
 * @method     KelompokQuery groupByNama() Group by the nama column
 * @method     KelompokQuery groupByAlamat() Group by the alamat column
 * @method     KelompokQuery groupByDesaId() Group by the desa_id column
 *
 * @method     KelompokQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     KelompokQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     KelompokQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     KelompokQuery leftJoinDesa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Desa relation
 * @method     KelompokQuery rightJoinDesa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Desa relation
 * @method     KelompokQuery innerJoinDesa($relationAlias = null) Adds a INNER JOIN clause to the query using the Desa relation
 *
 * @method     KelompokQuery leftJoinAnggota($relationAlias = null) Adds a LEFT JOIN clause to the query using the Anggota relation
 * @method     KelompokQuery rightJoinAnggota($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Anggota relation
 * @method     KelompokQuery innerJoinAnggota($relationAlias = null) Adds a INNER JOIN clause to the query using the Anggota relation
 *
 * @method     Kelompok findOne(PropelPDO $con = null) Return the first Kelompok matching the query
 * @method     Kelompok findOneOrCreate(PropelPDO $con = null) Return the first Kelompok matching the query, or a new Kelompok object populated from the query conditions when no match is found
 *
 * @method     Kelompok findOneByKelompokId(int $kelompok_id) Return the first Kelompok filtered by the kelompok_id column
 * @method     Kelompok findOneByNama(string $nama) Return the first Kelompok filtered by the nama column
 * @method     Kelompok findOneByAlamat(string $alamat) Return the first Kelompok filtered by the alamat column
 * @method     Kelompok findOneByDesaId(int $desa_id) Return the first Kelompok filtered by the desa_id column
 *
 * @method     array findByKelompokId(int $kelompok_id) Return Kelompok objects filtered by the kelompok_id column
 * @method     array findByNama(string $nama) Return Kelompok objects filtered by the nama column
 * @method     array findByAlamat(string $alamat) Return Kelompok objects filtered by the alamat column
 * @method     array findByDesaId(int $desa_id) Return Kelompok objects filtered by the desa_id column
 *
 * @package    propel.generator.mmrb.om
 */
abstract class BaseKelompokQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseKelompokQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'mmrb', $modelName = 'Kelompok', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new KelompokQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    KelompokQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof KelompokQuery) {
			return $criteria;
		}
		$query = new KelompokQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Kelompok|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = KelompokPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(KelompokPeer::KELOMPOK_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(KelompokPeer::KELOMPOK_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the kelompok_id column
	 * 
	 * @param     int|array $kelompokId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByKelompokId($kelompokId = null, $comparison = null)
	{
		if (is_array($kelompokId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(KelompokPeer::KELOMPOK_ID, $kelompokId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(KelompokPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the alamat column
	 * 
	 * @param     string $alamat The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByAlamat($alamat = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($alamat)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $alamat)) {
				$alamat = str_replace('*', '%', $alamat);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(KelompokPeer::ALAMAT, $alamat, $comparison);
	}

	/**
	 * Filter the query on the desa_id column
	 * 
	 * @param     int|array $desaId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByDesaId($desaId = null, $comparison = null)
	{
		if (is_array($desaId)) {
			$useMinMax = false;
			if (isset($desaId['min'])) {
				$this->addUsingAlias(KelompokPeer::DESA_ID, $desaId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($desaId['max'])) {
				$this->addUsingAlias(KelompokPeer::DESA_ID, $desaId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(KelompokPeer::DESA_ID, $desaId, $comparison);
	}

	/**
	 * Filter the query by a related Desa object
	 *
	 * @param     Desa $desa  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByDesa($desa, $comparison = null)
	{
		return $this
			->addUsingAlias(KelompokPeer::DESA_ID, $desa->getDesaId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Desa relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function joinDesa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Desa');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Desa');
		}
		
		return $this;
	}

	/**
	 * Use the Desa relation Desa object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    DesaQuery A secondary query class using the current class as primary query
	 */
	public function useDesaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinDesa($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Desa', 'DesaQuery');
	}

	/**
	 * Filter the query by a related Anggota object
	 *
	 * @param     Anggota $anggota  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function filterByAnggota($anggota, $comparison = null)
	{
		return $this
			->addUsingAlias(KelompokPeer::KELOMPOK_ID, $anggota->getKelompokId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Anggota relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function joinAnggota($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Anggota');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Anggota');
		}
		
		return $this;
	}

	/**
	 * Use the Anggota relation Anggota object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    AnggotaQuery A secondary query class using the current class as primary query
	 */
	public function useAnggotaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinAnggota($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Anggota', 'AnggotaQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Kelompok $kelompok Object to remove from the list of results
	 *
	 * @return    KelompokQuery The current query, for fluid interface
	 */
	public function prune($kelompok = null)
	{
		if ($kelompok) {
			$this->addUsingAlias(KelompokPeer::KELOMPOK_ID, $kelompok->getKelompokId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseKelompokQuery


# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- pengguna
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `pengguna`;


CREATE TABLE `pengguna`
(
	`pengguna_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(255)  NOT NULL,
	`password` VARCHAR(255)  NOT NULL,
	`nama` VARCHAR(255)  NOT NULL,
	`jabatan_id` INTEGER  NOT NULL,
	PRIMARY KEY (`pengguna_id`),
	INDEX `pengguna_FI_1` (`jabatan_id`),
	CONSTRAINT `pengguna_FK_1`
		FOREIGN KEY (`jabatan_id`)
		REFERENCES `jabatan` (`jabatan_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- jabatan
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `jabatan`;


CREATE TABLE `jabatan`
(
	`jabatan_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(10)  NOT NULL,
	PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- propinsi
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `propinsi`;


CREATE TABLE `propinsi`
(
	`propinsi_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50)  NOT NULL,
	PRIMARY KEY (`propinsi_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- kabkota
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `kabkota`;


CREATE TABLE `kabkota`
(
	`kabkota_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50)  NOT NULL,
	`propinsi_id` INTEGER  NOT NULL,
	PRIMARY KEY (`kabkota_id`),
	INDEX `kabkota_FI_1` (`propinsi_id`),
	CONSTRAINT `kabkota_FK_1`
		FOREIGN KEY (`propinsi_id`)
		REFERENCES `propinsi` (`propinsi_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- daerah
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `daerah`;


CREATE TABLE `daerah`
(
	`daerah_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50)  NOT NULL,
	`alamat` VARCHAR(255)  NOT NULL,
	`kabkota_id` INTEGER  NOT NULL,
	PRIMARY KEY (`daerah_id`),
	INDEX `daerah_FI_1` (`kabkota_id`),
	CONSTRAINT `daerah_FK_1`
		FOREIGN KEY (`kabkota_id`)
		REFERENCES `kabkota` (`kabkota_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- desa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `desa`;


CREATE TABLE `desa`
(
	`desa_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50)  NOT NULL,
	`alamat` VARCHAR(255)  NOT NULL,
	`daerah_id` INTEGER  NOT NULL,
	PRIMARY KEY (`desa_id`),
	INDEX `desa_FI_1` (`daerah_id`),
	CONSTRAINT `desa_FK_1`
		FOREIGN KEY (`daerah_id`)
		REFERENCES `daerah` (`daerah_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- kelompok
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `kelompok`;


CREATE TABLE `kelompok`
(
	`kelompok_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50)  NOT NULL,
	`alamat` VARCHAR(255)  NOT NULL,
	`desa_id` INTEGER  NOT NULL,
	PRIMARY KEY (`kelompok_id`),
	INDEX `kelompok_FI_1` (`desa_id`),
	CONSTRAINT `kelompok_FK_1`
		FOREIGN KEY (`desa_id`)
		REFERENCES `desa` (`desa_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- pekerjaan
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `pekerjaan`;


CREATE TABLE `pekerjaan`
(
	`pekerjaan_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`pekerjaan_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- anggota
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `anggota`;


CREATE TABLE `anggota`
(
	`anggota_id` VARCHAR(22)  NOT NULL,
	`nama` VARCHAR(255)  NOT NULL,
	`jenis_kelamin` INTEGER(2)  NOT NULL,
	`tempat_lahir` VARCHAR(50)  NOT NULL,
	`tanggal_lahir` DATE  NOT NULL,
	`pekerjaan_id` INTEGER  NOT NULL,
	`keterangan` VARCHAR(255)  NOT NULL,
	`alamat` VARCHAR(255)  NOT NULL,
	`tlp` VARCHAR(50)  NOT NULL,
	`foto` VARCHAR(255)  NOT NULL,
	`kelompok_id` INTEGER  NOT NULL,
	PRIMARY KEY (`anggota_id`),
	INDEX `anggota_FI_1` (`kelompok_id`),
	CONSTRAINT `anggota_FK_1`
		FOREIGN KEY (`kelompok_id`)
		REFERENCES `kelompok` (`kelompok_id`),
	INDEX `anggota_FI_2` (`pekerjaan_id`),
	CONSTRAINT `anggota_FK_2`
		FOREIGN KEY (`pekerjaan_id`)
		REFERENCES `pekerjaan` (`pekerjaan_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- pesan
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `pesan`;


CREATE TABLE `pesan`
(
	`pesan_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`isi` TEXT  NOT NULL,
	`anggota_id` VARCHAR(22)  NOT NULL,
	PRIMARY KEY (`pesan_id`),
	INDEX `pesan_FI_1` (`anggota_id`),
	CONSTRAINT `pesan_FK_1`
		FOREIGN KEY (`anggota_id`)
		REFERENCES `anggota` (`anggota_id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;

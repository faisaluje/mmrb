mmrb.recordSetKelompok = [{
	name: 'KelompokId',
	type: 'float'
}, {
	name: 'Nama',
	type: 'string'
}, {
	name: 'Alamat',
	type: 'string'
}, {
	name: 'DesaId',
	type: 'float'
}];

mmrb.KelompokStore = new Ext.data.Store({
	reader: new Ext.data.JsonReader({
		id: 'KelompokId',
		root: 'rows',
		totalProperty: 'results',
		fields: mmrb.recordSetKelompok
	}),
	autoLoad: true,
	url: './data/Kelompok.json',
	sortInfo: {
		field: 'KelompokId',
		direction: "ASC"
	}
});

mmrb.KelompokCombo = Ext.extend(Ext.form.ComboBox, {
	
	hiddenName: 'KelompokId',
	fieldLabel: 'Kelompok',
	emptyText: 'Pilih Kelompok...',
	// anchor: '100%',
	
	constructor: function(config){
		Ext.apply(this, config);
		
		Ext.apply(this, {			
			store: mmrb.KelompokStore,
			displayField: 'Nama',
			valueField: 'KelompokId',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			editable: false,
			selectOnFocus: true,
			// maxHeight: 200
		});
		
		mmrb.KelompokCombo.superclass.constructor.call(this);
	}

});

Ext.reg('kelompokcombo', mmrb.KelompokCombo); 